<?php
/**
 * A class/service for wrapping PostgreSQL's procedural functions
 * The goal is to be as lightweight as possible and reduce overhead
 * User: e_pickup
 * Date: 07/11/13
 */

namespace GorillaHub\DataConnectionBundle;
use \GorillaHub\DataConnectionBundle\Exceptions\ConnectionException;
use \GorillaHub\DataConnectionBundle\Exceptions\DuplicateRowException;
use \GorillaHub\DataConnectionBundle\Exceptions\QueryException;
use \GorillaHub\DataConnectionBundle\ResultSets\MySQLResultSet;
use \GorillaHub\GeneralBundle\PhpUnit;

/**
 * @package GorillaHub\DataConnectionBundle
 */
class MySQL extends AbstractSQLConnection {

	/**
	 * @var int An ID for the current transaction, if one is in progress.  This value is at least unique in the
	 *		current session (unless we have more than 2^64 transactions in one session).
	 */
	static private $transactionId = -1;

	/** @var \Mysqli The Mysqli object */
	private $connection = null;

	/** @var int The number of times a transaction has been entered, recursively. */
	private $transactionCount = 0;

	/**
	 * @var bool If a transaction is in progress, this is true if that transaction has failed and should be rolled
	 * 		back, or true if the transaction can be committed.
	 */
	private $transactionHasFailed;

	/**
	 * {@inheritdoc}
	 */
	public function connect() {
		if ($this->connection) {
			return;
		}
		$this->log(\json_encode(array(__METHOD__, func_get_args())),array('redis'));

		$this->host = $this->host ? $this->host : ini_get("mysqli.default_host");
		$this->port = $this->port ? $this->port : ini_get("mysqli.default_port");

		if (PhpUnit::isPhpUnitRunning() && $this->isTestDb() === false) {
			throw new \LogicException("Tried to connect to non-test DB from a unit test.");
		}

		try {
			if(!empty($this->sslOptions['priv_key']) && !empty($this->sslOptions['pub_cert'])  && !empty($this->sslOptions['ca_cert'])){
				$this->connection = mysqli_init();
				$this->connection->ssl_set($this->sslOptions['priv_key'],$this->sslOptions['pub_cert'],$this->sslOptions['ca_cert'],null, null);
				$this->connection->real_connect($this->host, $this->userName, $this->passWord, $this->database, $this->port);
			} else {
				$this->connection = new \mysqli($this->host, $this->userName,$this->passWord, $this->database, $this->port);
			}
		}
		catch(\Exception $e) {
			throw new ConnectionException("Unable to connect to MySQL on: " . $this->host . "Error: " . $e->getMessage());
		}
		$this->connection->query("SET NAMES '{$this->characterEncoding}';");

		if($this->timeZone!=null){
			$value = $this->connection->real_escape_string($this->timeZone);
			$this->connection->query("SET time_zone = '$value';");
		}
	}

	/**
	 * @param string $query
	 * @return MySQLResultSet
	 * @throws ConnectionException
	 * @throws DuplicateRowException
	 * @throws QueryException
	 */
	public function query($query) {
		$this->connect();
		$this->log(\json_encode(array(__METHOD__, func_get_args())),array('redis'));

		if (preg_match('`for\\s+update\\s*$`i', $query)) {
			if ($this->transactionCount === 0) {
				trigger_error(
					"Tried to do an operation FOR UPDATE when not in a transaction: " . $query,
					E_USER_WARNING
				);
			}
		}

		$result = $this->queryWithReconnect($query);
		if ($result === FALSE) {
			throw ($this->connection->errno == 1062)
					? new DuplicateRowException($this->connection->error . ': ' . $query)
					: new QueryException($this->connection->error . ': ' . $query);
		} else {
			return new MySQLResultSet($result);
		}
	}

	public function escapeIdentifier($objectName) {
		$this->connect();
		if (is_array($objectName)) {
			return implode('.', array_map(array($this, 'escapeIdentifier'), $objectName));
		} else {
			return  "`{$this->connection->real_escape_string($objectName)}`";
		}
	}

	public function escapeLiteral($value) {
		$this->connect();
		if (is_array($value)) {
			return '(' . implode(',', array_map(array($this, 'escapeLiteral'), $value)) . ')';
		}
		if ($value === null) {
			return 'NULL';
		}
		if ($value instanceof SQLExpression) {
			return $value->toString($this);
		} else if (is_int($value) || is_float($value)) {
			return (string)$value;
		}
		return "'" . $this->connection->real_escape_string($value) . "'";
	}

	public function getNumberOfUpdatedRows() {
		return $this->connection->affected_rows;
	}

	public function getLastInsertId() {
		return $this->connection->insert_id;
	}

	/**
	 * @param string $query
	 * @return string The first value in the first row.
	 * @throws ConnectionException
	 * @throws QueryException if there was not at least one row with at least one value.
	 */
	public function queryValue($query) {
		$resultSet = $this->query($query);
		$row = $resultSet->fetchAssociatedArray();
		if (is_array($row) === false) {
			throw new QueryException("At least one row expected in query " . $query);
		}
		$value = reset($row);
		if ($value === false) {
			throw new ConnectionException("At least one value expected in query " . $query);
		}
		return $value;
	}
	/**
	 * @return int The number of rows in FOUND_ROWS().
	 * @throws ConnectionException if the rows could not be retrieved.
	 */
	public function queryFoundRows() {
		return (int)$this->queryValue('SELECT FOUND_ROWS()');
	}
	/**
	 * {@inheritdoc}
	 * @return MySQLTransaction
	 */
	public function createTransaction() {
		return new MySQLTransaction($this);
	}

	public function startTransaction() {
		$this->connect();
		if ($this->transactionCount === 0) {
			$this->queryWithReconnect('START TRANSACTION');
			$this->transactionHasFailed = false;
			self::$transactionId++;
		}
		$this->transactionCount++;
		return self::$transactionId;
	}

	public function commit() {
		$this->endTransaction('commit');
	}

	public function rollBack() {
		$this->transactionHasFailed = true;
		$this->endTransaction('roll back');
	}

	public function isInTransaction() {
		return $this->transactionCount > 0;
	}

	public function isTestDb() {
		// TODO: Come up with a better way to do this so we don't need to keep adding hosts to run unit tests on other machines
		return $this->host === '127.255.255.255' || $this->host === 'mtl-tubes-mysql-testdb.mgcorp.co' || $this->host === 'mtl1-tubes-monitoring-staging.mgcorp.co';
	}

	public function selectDatabase($databaseName) {
		$result = $this->connection->select_db($databaseName);
		if ($result === false) {
			throw new QueryException($this->connection->error);
		}
	}

	private function endTransaction($endType) {
		$this->connect();
		if ($this->transactionCount === 0) {
			throw new \LogicException("Tried to $endType a transaction that was never started.");
		}
		if ($this->transactionCount === 1) {
			$this->queryWithReconnect($this->transactionHasFailed ? 'ROLLBACK' : 'COMMIT');
		}
		$this->transactionCount--;

	}

	private function queryWithReconnect($query) {
		$result = $this->connection->query($query);
		if ($result === false && $this->hasConnectionGoneAway() && $this->isInTransaction() === false) {
			$this->connection->close();
			$this->connection = null;
			$this->connect();
			return $this->connection->query($query);
		}
		return $result;
	}

	private function hasConnectionGoneAway() {
		return in_array($this->connection->errno, array(2006, 2013));
	}
}
