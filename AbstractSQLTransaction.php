<?php
namespace GorillaHub\DataConnectionBundle;

/**
 * An object of this class represents a database transaction.  When the object is created, the transaction is entered.
 * The transaction is committed when commit() is called, or it is rolled back when rollBack() is called or the object
 * is destructed.
 *
 * @package GorillaHub\DataConnectionBundle
 */
abstract class AbstractSQLTransaction
{
	abstract public function commit();
	abstract public function rollBack();
}