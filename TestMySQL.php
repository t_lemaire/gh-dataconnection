<?php
namespace GorillaHub\DataConnectionBundle;

/**
 * This is a subclass of MySQL that connects to a server that is set up for unit testing.
 *
 * @package GorillaHub\DataConnectionBundle
 */
class TestMySQL extends MySQL
{
	public function __construct($database = '') {
		parent::__construct('tester', 'dfw34R', '', 'mtl-tubes-mysql-testdb.mgcorp.co', 3306, 'UTF-8');
		if ($database !== '') {
			$this->query('CREATE DATABASE IF NOT EXISTS ' . $this->escapeIdentifier($database));
			$this->selectDatabase($database);
		}
	}
}
