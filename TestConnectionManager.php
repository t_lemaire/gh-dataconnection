<?php

namespace GorillaHub\DataConnectionBundle;

/**
 * A connection manager that returns the test MySQL.
 * @package GorillaHub\DataConnectionBundle
 */
class TestConnectionManager extends ConnectionManager {

	public function __construct($databaseName = '') {

		$db = new TestMySQL($databaseName);
		$this->addConnection('mysql', 'read', $db);
		$this->addConnection('mysql', 'write', $db);
	}

}
