<?php

namespace GorillaHub\DataConnectionBundle\DependencyInjection;

use \Symfony\Component\DependencyInjection\ContainerBuilder;
use \Symfony\Component\Config\FileLocator;
use \Symfony\Component\HttpKernel\DependencyInjection\Extension;
use \Symfony\Component\DependencyInjection\Loader;
use \Symfony\Component\DependencyInjection\Definition;
use \Symfony\Component\DependencyInjection\Reference;
use \Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class GorillaHubDataConnectionExtension extends Extension
{
	/**
	 * {@inheritDoc}
	 */
	public function load(array $configs, ContainerBuilder $container)
	{

		$configuration = new Configuration();
		$config        = $this->processConfiguration($configuration, $configs);

		$loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
		$loader->load('services.xml');

		foreach ($config['redis'] as $name => $connectionConfiguration) {
			$redisInstance = new Definition(
				'GorillaHub\DataConnectionBundle\Redis',
				array(
					$connectionConfiguration['password'], $connectionConfiguration['database'], $connectionConfiguration['host'], $connectionConfiguration['port'], $connectionConfiguration['connectiontype']
				)
			);
			$redisInstance->addMethodCall('setLogger', array(new Reference('logger', ContainerInterface::NULL_ON_INVALID_REFERENCE)));

			$container->setDefinition(sprintf('gorillahub_redis.connections.%s', $name), $redisInstance);
		}

		foreach ($config['mysql'] as $name => $connectionConfiguration) {
			$sslOptions    = array('priv_key' => $connectionConfiguration['priv_key'], 'pub_cert' => $connectionConfiguration['pub_cert'], 'ca_cert' => $connectionConfiguration['ca_cert']);
			$mysqlInstance = new Definition(
				'GorillaHub\DataConnectionBundle\MySQL',
				array(
					$connectionConfiguration['username'], $connectionConfiguration['password'], $connectionConfiguration['database'], $connectionConfiguration['host'], $connectionConfiguration['port'], $connectionConfiguration['characterencoding'], $sslOptions
				)
			);
			if (method_exists($mysqlInstance, 'setFactoryClass') && method_exists($mysqlInstance, 'setFactoryMethod')) {
				// BC for symfony/config < 3.0
				$mysqlInstance->setFactoryClass('GorillaHub\DataConnectionBundle\DependencyInjection\MySQLFactory');
				$mysqlInstance->setFactoryMethod('createMySQL');
			} else {
				$mysqlInstance->setFactory([new Definition('GorillaHub\DataConnectionBundle\DependencyInjection\MySQLFactory'), 'createMySQL']);
			}
			$mysqlInstance->addMethodCall('setLogger', array(new Reference('logger', ContainerInterface::NULL_ON_INVALID_REFERENCE)));

			$container->setDefinition(sprintf('gorillahub_mysql.connections.%s', $name), $mysqlInstance);
		}

		foreach ($config['postgresql'] as $name => $connectionConfiguration) {

			$postgresInstance = new Definition(
				'GorillaHub\DataConnectionBundle\PostgreSQL',
				array(
					$connectionConfiguration['username'], $connectionConfiguration['password'], $connectionConfiguration['database'], $connectionConfiguration['host'], $connectionConfiguration['port'], $connectionConfiguration['characterencoding']
				)
			);
			$postgresInstance->addMethodCall('setLogger', array(new Reference('logger', ContainerInterface::NULL_ON_INVALID_REFERENCE)));

			$container->setDefinition(sprintf('gorillahub_postgresql.connections.%s', $name), $postgresInstance);
		}
	}


}
