<?php

namespace GorillaHub\DataConnectionBundle\DependencyInjection;

use GorillaHub\DataConnectionBundle\MySQL;

class MySQLFactory
{
	/** @var MySQL[] */
	static private $mySQLInstances = [];

	static public function createMySQL(
		$userName,
		$passWord,
		$database,
		$host = "",
		$port = "",
		$characterEncoding = "",
		$sslOptions = array()
	) {
		if ($port === '') {
			$port = 3306;
		}
		$key = $host . ':' . $port;
		if (!isset(self::$mySQLInstances[$key])) {
			self::$mySQLInstances[$key] = new MySQL(
				$userName, $passWord, $database, $host, $port, $characterEncoding, $sslOptions
			);
		}
		return self::$mySQLInstances[$key];
	}

}