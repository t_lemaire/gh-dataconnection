<?php

namespace GorillaHub\DataConnectionBundle\DependencyInjection;

use \Symfony\Component\Config\Definition\Builder\TreeBuilder;
use \Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
	public function getConfigTreeBuilder()
	{
        $treeBuilder = new TreeBuilder('gorilla_hub_data_connection');
        if (method_exists($treeBuilder, 'getRootNode')) {
            $rootNode = $treeBuilder->getRootNode();
        } else {
            // BC for symfony/config < 4.2
            $rootNode = $treeBuilder->root('gorilla_hub_data_connection');
        }

		$rootNode
			->children()
				->arrayNode('redis')
					->useAttributeAsKey('name')
					->prototype('array')
						->children()
						->scalarNode('host')->end()
						->scalarNode('port')->defaultValue(3306)->end()
						->scalarNode('database')->defaultValue(0)->end()
						->scalarNode('password')->defaultValue('')->end()
						->scalarNode('persistent')->defaultValue('false')->end()
						->scalarNode('connectiontype')->defaultValue(\GorillaHub\DataConnectionBundle\Redis::CONNECTION_INET)->end()
						->end()
					->end()
				->end()
				->arrayNode('mysql')
					->useAttributeAsKey('name')
					->prototype('array')
						->children()
							->scalarNode('host')->end()
							->scalarNode('port')->defaultValue(3306)->end()
							->scalarNode('database')->end()
							->scalarNode('username')->end()
							->scalarNode('password')->end()
							->scalarNode('persistent')->defaultValue('false')->end()
							->scalarNode('characterencoding')->end()
                            ->scalarNode('priv_key')->end()
                            ->scalarNode('pub_cert')->end()
                            ->scalarNode('ca_cert')->end()
				        ->end()
				    ->end()
				->end()
				->arrayNode('postgresql')
					->useAttributeAsKey('name')
					->prototype('array')
						->children()
							->scalarNode('host')->end()
							->scalarNode('port')->defaultValue(5432)->end()
							->scalarNode('database')->end()
							->scalarNode('username')->end()
							->scalarNode('password')->end()
							->scalarNode('persistent')->defaultValue('false')->end()
							->scalarNode('characterencoding')->end()
				        ->end()
				    ->end()
				->end()
			->end();

		return $treeBuilder;
	}
}
