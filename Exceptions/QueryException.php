<?php
namespace GorillaHub\DataConnectionBundle\Exceptions;

/**
 * Thrown for errors with queries, generally malformed or attempting to unavailable resources.
 * @package GorillaHub\DataConnectionBundle\Exceptions
 */
class QueryException extends DataConnectionException {}