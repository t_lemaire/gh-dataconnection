<?php
namespace GorillaHub\DataConnectionBundle\Exceptions;

/**
 * Thrown when unable to connect to a datasource.
 * @package GorillaHub\DataConnectionBundle\Exceptions
 */
class ConnectionException extends DataConnectionException {}