<?php
namespace GorillaHub\DataConnectionBundle\Exceptions;

/**
 * A general exception generated when interacting with a datasource.
 * @package GorillaHub\DataConnectionBundle\Exceptions
 */
class DataConnectionException extends \Exception {}
