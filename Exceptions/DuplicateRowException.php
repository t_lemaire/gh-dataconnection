<?php
namespace GorillaHub\DataConnectionBundle\Exceptions;

/**
 * Thrown when an insert or update would violate a column uniqueness constraint.
 *
 * @package GorillaHub\DataConnectionBundle\Exceptions
 */
class DuplicateRowException extends \Exception {}
