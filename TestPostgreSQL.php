<?php
namespace GorillaHub\DataConnectionBundle;

/**
 * This is a subclass of PostgreSQL that connects to a server that is set up for unit testing.
 *
 * NOTE:  This is temporarily set up to connect to nayul-tubes-platform.mgcorp.co, because there is not yet any
 * dedicated PostgreSQL for unit tests.
 *
 * @package GorillaHub\DataConnectionBundle
 */
class TestPostgreSQL extends PostgreSQL
{
	public function __construct($database = '') {
		parent::__construct('platform', 'platform', $database, 'nayul-tubes-platform.mgcorp.co', 5432, 'UTF-8');
	}


}