<?php
namespace GorillaHub\DataConnectionBundle;
use \GorillaHub\DataConnectionBundle\Exceptions\ConnectionException;
use \GorillaHub\DataConnectionBundle\Exceptions\QueryException;
use \GorillaHub\DataConnectionBundle\ResultSets\PostgreSQLResultSet;
use \GorillaHub\GeneralBundle\PhpUnit;

/**
 * A class/service for wrapping PostgreSQL's procedural functions
 * The goal is to be as lightweight as possible and reduce overhead
 * @author e_pickup
 * @package GorillaHub\DataConnectionBundle
 */
class PostgreSQL extends AbstractSQLConnection {

	/**
	 * The connection to PostgreSQL
	 * @var resource
	 */
	private $connection = null;

	public function connect() {
		if ($this->connection) {
			return;
		}

		if (PhpUnit::isPhpUnitRunning() && $this->isTestDb() === false) {
			throw new \LogicException("Tried to connect to non-test DB from a unit test.");
		}

		$this->log(\json_encode(array(__METHOD__, func_get_args())),array('redis'));

		$connectionString = '';
		if($this->userName) {
			$connectionString .= 'user=' . $this->userName . ' ';
		}
		if($this->passWord) {
			$connectionString .= 'password=' . $this->passWord . ' ';
		}
		if($this->database) {
			$connectionString .= 'dbname=' . $this->database . ' ';
		}
		if($this->host) {
			$connectionString .= 'host=' . $this->host . ' ';
		}
		if($this->port) {
			$connectionString .= 'port=' . $this->port . ' ';
		}
		if($this->characterEncoding) {
			$connectionString .= "options='--client_encoding={$this->characterEncoding}'";
		}

		$this->connectByString($connectionString);
	}

	/**
	 * Connects to PostgreSQL using the default string format.
	 * @param string $connectionString
	 * @throws ConnectionException
	 */
	private function connectByString($connectionString) {
		if ($this->connection) {
			return;
		}

		@$this->connection = \pg_connect($connectionString);
		if (! $this->connection) {
			throw new ConnectionException('unable to connect: '. $connectionString);
		}
	}


	/**
	 * Executes an arbitrary query passed as a string.  Will connect to the DB if a valid connection doesn't
	 * already exist.
	 * @param string $query
	 * @return PostgreSQLResultSet
	 * @throws QueryException
	 */
	public function query($query) {
		$this->connect();
		$this->log(\json_encode(array(__METHOD__, func_get_args())),array('redis'));

		@$result = \pg_query($this->connection, $query);
		if ($result === FALSE) {
			throw new QueryException('invalid query: ' . $query);
		}
		else {
			return new PostgreSQLResultSet($result);
		}
	}

	/**
	 * {@inheritdoc}
	 * @param string $data
	 * @return string
	 */
	public function escapeIdentifier($data) {
		$this->connect();
		if (strpos($data, '.') > 0) {
			$parts = explode('.', $data);
			array_walk($parts, function($part, $i) {
				$parts[$i] = \pg_escape_identifier($this->connection, $part);
			});
			return implode('.', $parts);
		}
		return \pg_escape_identifier($this->connection, $data);
	}

	public function escapeLiteral($value) {
		$this->connect();
		if ($value === null) {
			return 'NULL';
		}
		if ($value instanceof SQLExpression || is_int($value) || is_float($value)) {
			return (string)$value;
		}
		return \pg_escape_literal($this->connection, $value);
	}

	public function getNumberOfUpdatedRows() {
		throw new \BadMethodCallException("getNumberOfUpdatedRows() is not yet implemented for PostgreSQL.");
	}

	public function getLastInsertId() {
		throw new \BadMethodCallException("getLastInsertId() is not yet implemented for PostgreSQL.");
	}

	public function createTransaction() {
		throw new \BadMethodCallException("createTransaction() is not yet implemented for PostgreSQL.");
	}

	public function startTransaction() {
		throw new \BadMethodCallException("startTransaction() is not yet implemented for PostgreSQL.");
	}

	public function commit() {
		throw new \BadMethodCallException("commit() is not yet implemented for PostgreSQL.");
	}

	public function rollBack() {
		throw new \BadMethodCallException("rollBack() is not yet implemented for PostgreSQL.");
	}

	public function isInTransaction() {
		throw new \BadMethodCallException("isInTransaction() is not yet implemented for PostgreSQL.");
	}

	public function selectDatabase($databaseName) {
		throw new \BadMethodCallException("selectDatabase() is not yet implemented for PostgreSQL.");
	}

	public function isTestDb() {
		return $this->host === '127.255.255.255' || $this->host === 'nayul-tubes-platform.mgcorp.co';
	}
}
