<?php
namespace GorillaHub\DataConnectionBundle\ResultSets;

/**
 * {@inheritdoc}
 * @package GorillaHub\DataConnectionBundle
 */
class PostgreSQLResultSet extends AbstractResultSet {
	/**
	 * A postgreSQL connection resource
	 * @var resource
	 */
	private $result;

	/**
	 * @param resource $result
	 */
	function __construct($result) {
		$this->result = $result;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getNumRows() {
		return \pg_num_rows($this->result);
	}

	/**
	 * {@inheritdoc}
	 */
	public function fetchRow() {
		return \pg_fetch_row($this->result);
	}

	/**
	 * {@inheritdoc}
	 */
	public function fetchAssociatedArray() {
		return \pg_fetch_assoc($this->result);
	}

	/**
	 * {@inheritdoc}
	 */
	public function fetchAll() {
		return \pg_fetch_all($this->result);
	}

	/**
	 * {@inheritdoc}
	 */
	public function fetchObject() {
		return \pg_fetch_object($this->result);
	}

	/**
	 * {@inheritdoc}
	 */
	public function free()  {
		return \pg_free_result($this->result);
	}

	/**
	 *{@inheritdoc}
	 */
	public function seek($offset) {
		return \pg_result_seek($this->result, $offset);
	}

}
