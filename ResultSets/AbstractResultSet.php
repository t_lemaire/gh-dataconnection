<?php
namespace GorillaHub\DataConnectionBundle\ResultSets;

/**
 * Object for interacting with a result set generated as the result of a query to the database that returns results.
 * Usually a SELECT but also DESC, etc.
 * @package GorillaHub\DataConnectionBundle
 */
abstract class AbstractResultSet {

	/**
	 * Returns the number of rows in the result set
	 * @return int
	 */
	abstract public function getNumRows();

	/**
	 * Returns next row of result set as an array
	 * @return array
	 */
	abstract public function fetchRow();

	/**
	 * @return array|false The next row of result set as an associative array, or false if none.
	 */
	abstract public function fetchAssociatedArray();

	/**
	 * Returns all results as an array of arrays
	 * @return array
	 */
	abstract public function fetchAll();

	/**
	 * Returns the next row of results as object
	 * @return object
	 */
	abstract public function fetchObject();

	/**
	 * Frees all memory used by the result set (both PHP and C level)
	 * @return mixed
	 */
	abstract public function free();

	/**
	 * Moves internal pointer to the row indicated by offset, next fetch___() method will return that row
	 * @param int
	 * @return bool
	 */
	abstract public function seek($offset);

}
