<?php
namespace GorillaHub\DataConnectionBundle\ResultSets;

/**
 * {@inheritdoc}
 * @package GorillaHub\DataConnectionBundle
 */
class MySQLResultSet extends AbstractResultSet {
	/**
	 * @var \mysqli_result $result
	 */
	private $result;

	/**
	 * @param \mysqli_result $result
	 */
	function __construct($result) {
		$this->result = $result;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getNumRows() {
		return $this->result->num_rows;
	}

	/**
	 * {@inheritdoc}
	 */
	public function fetchRow() {
		return $this->result->fetch_row();
	}

	/**
	 * {@inheritdoc}
	 */
	public function fetchAssociatedArray() {
		return $this->result->fetch_assoc();
	}

	/**
	 * {@inheritdoc}
	 */
	public function fetchAll() {
		if (method_exists($this->result, 'fetch_all')) {
			return $this->result->fetch_all(MYSQLI_ASSOC);
		} else {
			$rows = array();
			for (;;) {
				$row = $this->result->fetch_assoc();
				if ($row === null) {
					return $rows;
				}
				$rows[] = $row;
			}
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function fetchObject() {
		return $this->result->fetch_object();

	}

	/**
	 * {@inheritdoc}
	 */
	public function free()  {
		return $this->result->free();

	}

	/**
	 * {@inheritdoc}
	 */
	public function seek($offset) {
		return $this->result->data_seek($offset);

	}

}
