<?php
namespace GorillaHub\DataConnectionBundle\SQLExpressions;
use GorillaHub\DataConnectionBundle\AbstractSQLConnection;
use GorillaHub\DataConnectionBundle\SQLExpression;

/**
 * This represents an SQL expression that identifies a specific element by one or more columns, and/or all elements
 * that are "greater" than it, and/or all elements that are "lesser" than it.  For example, given
 *
 *      ["col1" => "val1", "col2" => "val2", "col3" => "val3"]
 *
 * and the operator ">", an instance of this class is equivalent to the expression
 *
 *      (`col1`, `col2`, `col3`) >= ("val1", "val2", "val3")
 *
 * except that it is written to help MySQL use the right index.
 */
class ContinuationExpression extends SQLExpression
{
    /** @var mixed[] The values at which to continue, in order from major to minor, indexed by column name. */
    public $valuesByColumn;

    /** @var string An operator, '=', '>', '>=', '<', or '<='. */
    public $operator;

    /**
     * @param mixed[] $valuesByColumn The values at which to continue, in order from major to minor, indexed by
     *      column name.
     * @param string $operator An operator, '=', '>', '>=', '<', or '<='.
     */
    public function __construct($valuesByColumn, $operator = '>') {
        parent::__construct('');
        $this->valuesByColumn = $valuesByColumn;
        $this->operator = $operator;
    }

    public function toString(AbstractSQLConnection $sqlConnection) {
        if ($this->operator === '=') {
            return '(' . $sqlConnection->prepareExpression($this->valuesByColumn) . ')';
        } else {
            $orClauses = [];
            $equalColumns = [];
            foreach ($this->valuesByColumn as $column => $value) {
                $andClauses = [];
                foreach ($equalColumns as $eqColumn => $eqValue) {
                    $andClauses[] = $this->pair($sqlConnection, $eqColumn, '=', $eqValue);
                }
                $andClauses[] = $this->pair($sqlConnection, $column, $this->getOperator($equalColumns), $value);
                $orClauses[] = '(' . implode(' AND ', $andClauses) . ')';
                $equalColumns[$column] = $value;
            }
            return '(' . implode(' OR ', $orClauses) . ')';
        }
    }

   	function __tostring() {
        throw new \LogicException("Cannot convert " . get_class() . " to a string.");
        return '';
   	}

   	private function pair(AbstractSQLConnection $sqlConnection, $column, $operator, $value) {
        return $sqlConnection->escapeIdentifier($column) . ' ' . $operator . ' '
                . $sqlConnection->escapeLiteral($value);
    }

    /**
     * @param mixed[] $equalColumns
     * @return string The operator to use for the last column.  If $equalColumns is all but one column, then this
     *      is the requested operator.  Otherwise it is the strict version of the inequality.
     */
    private function getOperator($equalColumns) {
        if (count($equalColumns) + 1 === count($this->valuesByColumn)) {
            return $this->operator;
        }
        return substr($this->operator, 0, 1);
    }
}