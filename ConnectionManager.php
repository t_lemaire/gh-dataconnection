<?php

namespace GorillaHub\DataConnectionBundle;
use GorillaHub\DataConnectionBundle\Helpers\LoggerAdapter;
use \Symfony\Bridge\Monolog\Logger;
/**
 * A connection manager that returns connection resources as needed.
 * @package GorillaHub\DataConnectionBundle
 */
class ConnectionManager {

	/**
	 * A array of connection ojbects
	 * @var array
	 */
	private $connections = array();
	/**
	 * @var LoggerAdapter
	 */
	private $logger;

	/**
	 * Add a connection specifying it's type.  Generally this will be done by the dependency management system.
	 * @param string $name
	 * @param string $type
	 * @param AbstractConnection $connection
	 * @throws InvalidConnectionTypeException
	 */
	public function addConnection($name = '', $type = '', AbstractConnection $connection) {
		if (! in_array($name, array('mysql', 'redis', 'postgresql'))) {
			throw new InvalidConnectionTypeException("$name not a valid connection");
		}
		$this->connections[$name][$type] = $connection;
	}

	/**
	 * @param Logger|null $logger
	 */
	public function setLogger(Logger $logger = null) {
		$this->logger = new LoggerAdapter($logger);
	}

	/**
	 * Returns a connection casted to a Redis Ojbect.
	 * @param string $type
	 * @return Redis
	 */
	public function getRedisConnection($type = '') {

		return $this->connections['redis'][$type];
	}

	/**
	 * Returns a connection casted to a MySQL Ojbect.
	 * @param string $type
	 * @return MySQL
	 */
	public function getMySQLConnection($type = '') {

		return $this->connections['mysql'][$type];
	}

	/**
	 * Returns a connection casted to a PostgreSQL Ojbect.
	 * @param string $type
	 * @return PostgreSQL
	 */
	public function getPostgreSQLConnection($type = '') {

		return $this->connections['postgresql'][$type];
	}
}

class InvalidConnectionTypeException extends \Exception {}