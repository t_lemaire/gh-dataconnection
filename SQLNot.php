<?php

namespace GorillaHub\DataConnectionBundle;

/**
 * An object of this class represents the complement of a set of possible items.  An object of this type
 * can be passed to certain functions that construct queries, for example AbstractSQLConnection's
 * prepareExpression function.
 *
 * @package GorillaHub\DataConnectionBundle
 */
class SQLNot
{
	private $value;

	/**
	 * @param mixed $value
	 */
	public function __construct($value) {
		$this->value = $value;
	}

	public function getValue() {
		return $this->value;
	}
}