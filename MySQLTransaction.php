<?php
namespace GorillaHub\DataConnectionBundle;

/**
 * @package GorillaHub\DataConnectionBundle
 */
class MySQLTransaction extends AbstractSQLTransaction
{
	/** @var MySQL */
	private $mySQL;

	/** @var bool True if the transaction needs to be ended, or false otherwise. */
	private $isInTransaction = true;

	/**
	 * @var int An integer that uniquely identifies this transaction, within this session.  In the case of nested
	 * 		transactions, all simultaneous transactions (i.e. all transactions that appear to be the same transaction
	 * 		from the perspective of the database) have the same ID.
	 */
	private $transactionId;

	public function __construct(MySQL $mySQL) {
		$this->mySQL = $mySQL;
		$this->transactionId = $mySQL->startTransaction();
	}

	public function __destruct() {
		try {
			if ($this->isInTransaction) {
				$this->mySQL->rollBack();
			}
		} catch (\Exception $e) {
		}
	}

	public function commit() {
		$this->isInTransaction = false;
		$this->mySQL->commit();
	}

	public function rollBack() {
		$this->isInTransaction = false;
		$this->mySQL->rollBack();
	}

	public function getId() {
		return $this->transactionId;
	}
}
