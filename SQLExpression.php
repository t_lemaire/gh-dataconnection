<?php

namespace GorillaHub\DataConnectionBundle;

/**
 * An object of this class represents an SQL expression that can be inserted into an SQL string as-is, without escaping
 * or conversion to an SQL literal that represents the string.
 *
 * @package GorillaHub\DataConnectionBundle
 */
class SQLExpression
{
	private $_string;

	function __construct($string) {
		$this->_string = $string;
	}

	public function toString(AbstractSQLConnection $sqlConnection) {
		return $this->_string;
	}

	function __tostring() {
		return $this->_string;
	}
}