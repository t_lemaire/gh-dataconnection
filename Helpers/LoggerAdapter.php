<?php
namespace GorillaHub\DataConnectionBundle\Helpers;

use \Symfony\Bridge\Monolog\Logger;

/**
 * This class allows us to use Monolog 1.x and 2.x.  It detects which version is installed and delegates to
 * the appropriate methods.
 */
class LoggerAdapter
{
    /** @var Logger|null The actual Monolog logger, v1.x or v2.x, or null if no logging should occur. */
    private $logger;

    public function __construct(Logger $logger = null) {
        $this->logger = $logger;
    }

    /**
     * Adds a log record at the DEBUG level.
     *
     * @param  string  $message The log message
     * @param  array   $context The log context
     * @return Boolean Whether the record has been processed
     */
    public function addDebug($message, $context) {
        if ($this->logger !== null) {
            if (method_exists($this->logger, 'addDebug')) {
                return $this->logger->addDebug($message, $context);
            } else {
                return $this->logger->debug($message, $context);
            }
        } else {
            return true;
        }
    }
}

