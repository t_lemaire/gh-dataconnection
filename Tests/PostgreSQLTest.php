<?php
namespace GorillaHub\DataConnectionBundle\Tests;
use \GorillaHub\DataConnectionBundle\TestPostgreSQL;
use \GorillaHub\DataConnectionBundle\PostgreSQL;

class PostgreSQLTest extends \PHPUnit_Framework_TestCase {

	public function testEscapeIdentifier() {
		$postgres = new TestPostgreSQL('platform');

		$this->assertEquals($postgres->escapeIdentifier('eric'), '"eric"');
	}

	public function testEscapeIdentifiers() {
		$postgres = new TestPostgreSQL('platform');

		$this->assertEquals($postgres->escapeIdentifiers(array('eric')), array('"eric"'));
		$this->assertEquals($postgres->escapeIdentifiers(array('eric', 'tes"t')), array('"eric"', '"tes""t"'));
	}

	/**
	* @dataProvider testEscapeLiteralProvider
	*/
	public function testEscapeLiteral($a, $b) {
		$postgres = new TestPostgreSQL('platform');

		$this->assertEquals($postgres->escapeLiteral($a), $b);
	}

	public function testEscapeLiteralProvider() {
		return array(
			array('eric', "'eric'"),
			array("eric's test", "'eric''s test'"),
			array("eric', do_something();", "'eric'', do_something();'"),
		  );
	}
	/**
	* @dataProvider testEscapeLiteralsProvider
	*/
	public function testEscapeLiterals($a, $b) {
		$postgres = new TestPostgreSQL('platform');

		$this->assertEquals($postgres->escapeLiterals($a), $b);
	}

	public function testEscapeLiteralsProvider() {
		return array(
			array(array('eric'), array("'eric'")),
			array(array("eric's test", 'eric'), array("'eric''s test'", "'eric'")),
			array(array("eric', do_something();", "eric's test", 'eric'), array("'eric'', do_something();'", "'eric''s test'", "'eric'")),
		  );
	}
	/**
	* @expectedException \GorillaHub\DataConnectionBundle\Exceptions\ConnectionException
	*/
	public function testConnectException() {
		$postgres = new PostgreSQL('e_pickup','', 'testdsds', '127.255.255.255');

		$postgres->connect();
	}

	/**
	* @expectedException \GorillaHub\DataConnectionBundle\Exceptions\QueryException
	*/
	public function testQueryException() {
		$postgres = new PostgreSQL('platform', 'platform', 'platform', 'nayul-tubes-platform.mgcorp.co');

		$postgres->query("SELECT das FORM");
	}

	public function testQuery() {
		$postgres = new PostgreSQL('platform', 'platform', 'platform', 'nayul-tubes-platform.mgcorp.co');

		$this->assertInstanceOf('\GorillaHub\DataConnectionBundle\ResultSets\PostgreSQLResultSet', $postgres->query("SELECT 1"));
	}

	public function testPrepareInsert() {
		$postgres = new PostgreSQL('platform', 'platform', 'platform', 'nayul-tubes-platform.mgcorp.co');

		$this->assertEquals($postgres->prepareInsert('my_table',
							  array('col1' => 'val1', 'col2' => 'val2')), "INSERT INTO \"my_table\" ( \"col1\", \"col2\" ) VALUES ( 'val1', 'val2' )");

		$this->assertEquals($postgres->prepareInsert('my_table',
							  array('col1' => 'val1', 'col2' => 'val2'), 'REPLACE'), "REPLACE INTO \"my_table\" ( \"col1\", \"col2\" ) VALUES ( 'val1', 'val2' )");

		$this->assertEquals($postgres->prepareInsert('my_table',
							  array('col1' => 'val1', 'col2' => 'val2'),
							  'REPLACE',
							  array('col2')), "REPLACE INTO \"my_table\" ( \"col1\", \"col2\" ) VALUES ( 'val1', 'val2' ) ON DUPLICATE KEY UPDATE \"col2\" = VALUES(\"col2\")");

	}

	public function testPrepareUpdate() {
		$postgres = new PostgreSQL('platform', 'platform', 'platform', 'nayul-tubes-platform.mgcorp.co');

		$this->assertEquals($postgres->prepareUpdate('my_table',
							  array('col1' => 'val1', 'col2' => 'val2'), 'col1'), "UPDATE \"my_table\" SET \"col2\" = 'val2' WHERE \"col1\" = 'val1'");

	}

	/**
	* @expectedException \GorillaHub\DataConnectionBundle\Exceptions\QueryException
	*/
	public function testPrepareUpdateException() {
		$postgres = new PostgreSQL('platform', 'platform', 'platform', 'nayul-tubes-platform.mgcorp.co');

		$this->assertEquals($postgres->prepareUpdate('my_table',
							  array('col1' => 'val1', 'col2' => 'val2'), 'colXXX'), "UPDATE \"my_table\" SET \"col2\" = 'val2' WHERE \"col\" = 'val1'");

	}

}
