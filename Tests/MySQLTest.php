<?php
namespace GorillaHub\DataConnectionBundle\Tests;
use \GorillaHub\DataConnectionBundle\TestMySQL;
use \GorillaHub\DataConnectionBundle\MySQL;

class MySQLTest extends \PHPUnit_Framework_TestCase {

	public function testEscapeIdentifier() {
		$mysql = new TestMySQL();

		$this->assertEquals($mysql->escapeIdentifier('eric'), "`eric`");
	}

	public function testEscapeIdentifiers() {
		$mysql = new TestMySQL();

		$this->assertEquals($mysql->escapeIdentifiers(array('eric')), array("`eric`"));
		$this->assertEquals($mysql->escapeIdentifiers(array('eric', 'tes"t')), array("`eric`", '`tes\"t`'));
	}

	/**
	* @dataProvider testEscapeLiteralProvider
	*/
	public function testEscapeLiteral($a, $b) {
		$mysql = new TestMySQL();

		$this->assertEquals($mysql->escapeLiteral($a), $b);
	}

	public function testEscapeLiteralProvider() {
		return array(
			array('eric', "'eric'"),
			array("eric's test", "'eric\\'s test'"),
			array("eric', do_something();", "'eric\\', do_something();'"),
		  );
	}
	/**
	* @dataProvider testEscapeLiteralsProvider
	*/
	public function testEscapeLiterals($a, $b) {
		$mysql = new TestMySQL();

		$this->assertEquals($mysql->escapeLiterals($a), $b);
	}

	public function testEscapeLiteralsProvider() {
		return array(
			array(array('eric'), array("'eric'")),
			array(array("eric's test", 'eric'), array("'eric\'s test'", "'eric'")),
			array(array("eric', do_something();", "eric's test", 'eric'), array("'eric\\', do_something();'", "'eric\'s test'", "'eric'")),
		  );
	}
	/**
	* @expectedException \GorillaHub\DataConnectionBundle\Exceptions\ConnectionException
	*/
	public function testConnectException() {
		$mysql = new MySQL('e_pickup','', 'testdsds', '127.255.255.255');

		$mysql->connect();
	}

	/**
	* @expectedException \GorillaHub\DataConnectionBundle\Exceptions\QueryException
	*/
	public function testQueryException() {
		$mysql = new TestMySQL();

		$mysql->query("SELECT das FORM");
	}

	public function testQuery() {
		$mysql = new TestMySQL();

		$this->assertInstanceOf('\GorillaHub\DataConnectionBundle\ResultSets\MySQLResultSet', $mysql->query("SELECT 1"));
	}

	public function testPrepareInsert() {
		$mysql = new TestMySQL();

		$this->assertEquals(
			"INSERT INTO `my_table` ( `col1`, `col2` ) VALUES ( 'val1', 'val2' )",
			$mysql->prepareInsert('my_table', array('col1' => 'val1', 'col2' => 'val2'))
		);
		$this->assertEquals(
			"REPLACE INTO `my_table` ( `col1`, `col2` ) VALUES ( 'val1', 'val2' )",
			$mysql->prepareInsert('my_table', array('col1' => 'val1', 'col2' => 'val2'), 'REPLACE')
		);
		$this->assertEquals(
			"REPLACE INTO `my_table` ( `col1`, `col2` ) VALUES ( 'val1', 'val2' ) ON DUPLICATE KEY UPDATE `col2` = VALUES(`col2`)",
			$mysql->prepareInsert('my_table', array('col1' => 'val1', 'col2' => 'val2'), 'REPLACE', array('col2'))
		);
	}

	public function testPrepareUpdate() {
		$mysql = new TestMySQL();

		$this->assertEquals(
			"UPDATE `my_table` SET `col2` = 'val2' WHERE `col1` = 'val1'",
			$mysql->prepareUpdate(
				'my_table',
				 array('col1' => 'val1', 'col2' => 'val2'),
				'col1'
			)
		);
		$this->assertEquals(
			"UPDATE `my_table` SET `col3` = 'val3' WHERE `col1` = 'val1' AND `col2` = 'val2'",
			$mysql->prepareUpdate(
				'my_table',
				 array('col1' => 'val1', 'col2' => 'val2', 'col3' => 'val3'),
				array('col1', 'col2')
			)
		);

	}

	/**
	* @expectedException \GorillaHub\DataConnectionBundle\Exceptions\QueryException
	*/
	public function testPrepareUpdateException() {
		$mysql = new TestMySQL();

		$this->assertEquals($mysql->prepareUpdate('my_table',
							  array('col1' => 'val1', 'col2' => 'val2'), 'colXXX'), "UPDATE 1my_table1 SET `col2` = 'val2' WHERE `col1` = 'val1'");

	}

}
