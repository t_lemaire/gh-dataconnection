<?php


namespace GorillaHub\DataConnectionBundle\Tests\ResultSets;
use \GorillaHub\DataConnectionBundle\ResultSets\MySQLResultSet;
use \GorillaHub\DataConnectionBundle\MySQL;
use \GorillaHub\DataConnectionBundle\TestMySQL;


class MySQLResultSetTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var MySQL
	 */
	private $mysql;
	/**
	 * {@inheritdoc}
	 */
	public function setUp() {
		parent::setUp();
		//DROP TABLE IF EXISTS `test_table`;CREATE TABLE `test_table` (`primary_key` int(11) NOT NULL, `int1` int(11) DEFAULT NULL,`varchar1` varchar(255) CHARACTER SET utf8 DEFAULT NULL, `int2` int(11) DEFAULT NULL, `int3` int(11) DEFAULT NULL, PRIMARY KEY (`primary_key`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;INSERT INTO `test_table` VALUES (0,NULL,NULL,NULL,NULL),(1,1000,'eric',1,3),(2,789789,'james',4,5),(3,2334,'pavel',87,33),(4,56,'bob',898908,23);
		$this->mysql = new TestMySQL('test');
		$this->mysql->query("DROP TABLE IF EXISTS `test_table`;");
		$this->mysql->query("CREATE TABLE `test_table` (`primary_key` int(11) NOT NULL, `int1` int(11) DEFAULT NULL,`varchar1` varchar(255) CHARACTER SET utf8 DEFAULT NULL, `int2` int(11) DEFAULT NULL, `int3` int(11) DEFAULT NULL, PRIMARY KEY (`primary_key`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");
		$this->mysql->query("INSERT INTO `test_table` VALUES (1,1000,'eric',1,3),(2,789789,'james',4,5),(3,2334,'pavel',87,33),(4,56,'bob',898908,23);");
	}
	/**
	 * {@inheritdoc}
	 */
	public function tearDown() {
		parent::tearDown();
		$this->mysql->query("DROP TABLE IF EXISTS `test_table`;");
	}

	public function testGetNumRows() {
		$resultSet = $this->mysql->query("SELECT * from test_table");

		$this->assertEquals($resultSet->getNumRows(), 4);
	}

	public function testfetchRow() {
		$resultSet = $this->mysql->query("SELECT * from test_table ORDER BY primary_key ASC");
		$this->assertEquals($resultSet->fetchRow(), array(1,1000,'eric',1,3));
		$this->assertEquals($resultSet->fetchRow(), array(2,789789,'james',4,5));
		$this->assertEquals($resultSet->fetchRow(), array(3,2334,'pavel',87,33));
		$this->assertEquals($resultSet->fetchRow(), array(4,56,'bob',898908,23));
	}

	public function testFetchAssociatedArray() {
		$resultSet = $this->mysql->query("SELECT * from test_table ORDER BY primary_key ASC");
		$this->assertEquals($resultSet->fetchAssociatedArray(), array('primary_key' => 1,'int1' => 1000, 'varchar1' => 'eric', 'int2' => 1, 'int3' => 3));
		$this->assertEquals($resultSet->fetchAssociatedArray(), array('primary_key' => 2,'int1' => 789789, 'varchar1' => 'james', 'int2' => 4, 'int3' => 5));
		$this->assertEquals($resultSet->fetchAssociatedArray(), array('primary_key' => 3,'int1' => 2334, 'varchar1' => 'pavel', 'int2' => 87, 'int3' => 33));
		$this->assertEquals($resultSet->fetchAssociatedArray(), array('primary_key' => 4,'int1' => 56, 'varchar1' => 'bob', 'int2' => 898908, 'int3' => 23));
	}

	public function testFetchAll() {
		$resultSet = $this->mysql->query("SELECT * from test_table ORDER BY primary_key ASC");
		$this->assertEquals($resultSet->fetchAll(), array(
														array('primary_key' => 1,'int1' => 1000, 'varchar1' => 'eric', 'int2' => 1, 'int3' => 3),
														array('primary_key' => 2,'int1' => 789789, 'varchar1' => 'james', 'int2' => 4, 'int3' => 5),
														array('primary_key' => 3,'int1' => 2334, 'varchar1' => 'pavel', 'int2' => 87, 'int3' => 33),
														array('primary_key' => 4,'int1' => 56, 'varchar1' => 'bob', 'int2' => 898908, 'int3' => 23))
													);
	}

	public function testfetchObject() {
		$resultSet = $this->mysql->query("SELECT * from test_table ORDER BY primary_key ASC");

		$this->assertEquals($resultSet->fetchObject(), unserialize('O:8:"stdClass":5:{s:11:"primary_key";s:1:"1";s:4:"int1";s:4:"1000";s:8:"varchar1";s:4:"eric";s:4:"int2";s:1:"1";s:4:"int3";s:1:"3";}'));
		$this->assertEquals($resultSet->fetchObject(), unserialize('O:8:"stdClass":5:{s:11:"primary_key";s:1:"2";s:4:"int1";s:6:"789789";s:8:"varchar1";s:5:"james";s:4:"int2";s:1:"4";s:4:"int3";s:1:"5";}'));
		$this->assertEquals($resultSet->fetchObject(), unserialize('O:8:"stdClass":5:{s:11:"primary_key";s:1:"3";s:4:"int1";s:4:"2334";s:8:"varchar1";s:5:"pavel";s:4:"int2";s:2:"87";s:4:"int3";s:2:"33";}'));
		$this->assertEquals($resultSet->fetchObject(), unserialize('O:8:"stdClass":5:{s:11:"primary_key";s:1:"4";s:4:"int1";s:2:"56";s:8:"varchar1";s:3:"bob";s:4:"int2";s:6:"898908";s:4:"int3";s:2:"23";}'));
	}

	public function testSeekAndFree() {
		$resultSet = $this->mysql->query("SELECT * from test_table ORDER BY primary_key ASC");
		$this->assertEquals($resultSet->fetchRow(), array(1,1000,'eric',1,3));
		$resultSet->seek(0);
		$this->assertEquals($resultSet->fetchRow(), array(1,1000,'eric',1,3));
		$resultSet->seek(3);
		$this->assertEquals($resultSet->fetchRow(), array(4,56,'bob',898908,23));
		// since it has no side effects, testing free() means it doesn't crash :)
		$resultSet->free();
	}
}
