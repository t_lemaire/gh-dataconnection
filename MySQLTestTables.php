<?php
namespace GorillaHub\DataConnectionBundle;

use \GorillaHub\GeneralBundle\Php;

/**
 * @package GorillaHub\DataConnectionBundle
 */
class MySQLTestTables
{
	private $cachedCreateTableStatements = array();

	/** @var MySQL */
	private $db;

	/** @var array|string The ID of the locks table {@see AbstractSQLConnection::escapeIdentifier} */
	private $locksTableId = array('phpunit', 'locks');

	/** @var string The ID of the locks table, as a quoted SQL identifier */
	private $quotedLocksTableId;

	const LOCK_EXPIRE_SECONDS = 10;
	const LOCK_RETRY_SECONDS = 30;
	const LOCK_RETRY_INTERVAL_SECONDS = 0.5;

	public function __construct(MySQL $db) {
		if ($db->isTestDb() === false) {
			throw new \Exception("Tried to create test tables on a non-test DB.");
		}
		$this->db = $db;
		$this->quotedLocksTableId = $this->db->escapeIdentifier($this->locksTableId);
		$this->lock();
	}

	public function __destruct() {
		$this->unlock();
	}

	/**
	 * @param string|array $tableIdentifier
	 * @param string $createTableStatement
	 * @throws \Exception if the database is not a test database.
	 */
	public function createTestTable($tableIdentifier, $createTableStatement) {
		$quotedId = $this->db->escapeIdentifier($tableIdentifier);
		$truncateIsSupported = $this->isTruncateSupported($createTableStatement);
		if ($truncateIsSupported && $this->isTableCached($quotedId, $createTableStatement)) {
			$this->db->query('TRUNCATE TABLE ' . $quotedId);
			return;
		}
		$existingCreateSql = $this->getExistingCreateSql($quotedId);
		if ($truncateIsSupported && $this->areCreateTablesSame($existingCreateSql, $createTableStatement)) {
			$this->db->query('TRUNCATE TABLE ' . $quotedId);
			$this->cachedCreateTableStatements[$quotedId] = $existingCreateSql;
			return;
		}
		try {
			if ($existingCreateSql !== null) {
				$this->db->query('DROP TABLE ' . $quotedId);
			}
			$this->db->query($createTableStatement);
		} catch (\Exception $e) {
			if (!is_array($tableIdentifier) || !isset($tableIdentifier[1])) {
				throw $e;
			}
			$this->db->query('CREATE DATABASE IF NOT EXISTS' . $this->db->escapeIdentifier($tableIdentifier[0]));
			$this->db->query($createTableStatement);
		}
		$existingCreateSql = $this->getExistingCreateSql($quotedId);
		if ($this->areCreateTablesSame($existingCreateSql, $createTableStatement) === false) {
			trigger_error(
				'CREATE TABLE statement for ' . $quotedId . ' is not the same as returned by the RDBMS.'
						. ' The table will be re-created for each test.',
				E_USER_WARNING
			);
		}
		$this->cachedCreateTableStatements[$quotedId] = $createTableStatement;
	}

	private function isTableCached($quotedId, $createTableStatement) {
		if (!isset($this->cachedCreateTableStatements[$quotedId])) {
			return false;
		}
		return $this->areCreateTablesSame($createTableStatement, $this->cachedCreateTableStatements[$quotedId]);
	}

	private function areCreateTablesSame($createTableSql1, $createTableSql2) {
		$normal1 = $this->normalizeCreateTable($createTableSql1);
		$normal2 = $this->normalizeCreateTable($createTableSql2);
		return $normal1 === $normal2;
	}

	private function normalizeCreateTable($createTableSql) {
		if (!is_string($createTableSql)) {
			return $createTableSql;
		}
		preg_match('`^[^(]*(\\(.*)$`s', $createTableSql, $matches);
		if (!isset($matches[1])) {
			throw new \Exception("Unexpected format of CREATE TABLE: " . $createTableSql);
		}
		$sql = preg_replace('`\\bAUTO_INCREMENT=([0-9]+)`', '', $matches[1]);
		return trim(preg_replace('`\\s+`', ' ', $sql));
	}

	private function getExistingCreateSql($quotedId) {
		try {
			$result = $this->db->query('SHOW CREATE TABLE ' . $quotedId);
			$rows = $result->fetchAll();
			return Php::valueOrNull($rows[0]['Create Table']);
		} catch (\Exception $e) {

			return null;
		}
	}

	private function lock() {
		if (isset($this->locksTableId[1])) {
			$this->db->query(
				'CREATE DATABASE IF NOT EXISTS ' . $this->db->escapeIdentifier($this->locksTableId[0])
						. ' CHARACTER SET = "utf8"'
			);
		}
		$this->db->query(
			'CREATE TABLE IF NOT EXISTS ' . $this->quotedLocksTableId . ' (
					  `host` varchar(255) NOT NULL,
					  `pid` int(10) unsigned NOT NULL,
					  `time` int(10) unsigned NOT NULL
					) ENGINE=InnoDB DEFAULT CHARSET=utf8
					'
		);
		$endTime = time() + self::LOCK_RETRY_SECONDS;
		while (time() < $endTime) {
			$gotLock = $this->tryToAcquireLock();
			if ($gotLock) {
				return;
			}
			usleep((int)(self::LOCK_RETRY_INTERVAL_SECONDS * 1000000));
		}
		throw new \Exception("Can't lock the test database.");
	}

	private function tryToAcquireLock() {
		$transaction = $this->db->createTransaction();
		$rows = $this->db->query('SELECT * FROM ' . $this->quotedLocksTableId . ' FOR UPDATE')->fetchAll();
		$isLocked = isset($rows[0]['time']);
		$lockTimedOut = $isLocked && (time() > $rows[0]['time'] + self::LOCK_EXPIRE_SECONDS);
		if ($isLocked && $lockTimedOut === false) {
			return false;
		}
		if ($lockTimedOut) {
			$this->db->query('TRUNCATE TABLE ' . $this->quotedLocksTableId);
		}
		$row = array(
			'host' => php_uname('n'),
			'pid' => getmypid(),
			'time' => time()
		);
		$this->db->query($this->db->prepareInsert($this->locksTableId, $row));
		$transaction->commit();
		return true;
	}

	private function unlock() {
		$this->db->query('TRUNCATE TABLE ' . $this->quotedLocksTableId);
	}

	private function isTruncateSupported($createTableSql) {
		return preg_match('`\\bengine\\s*=\\s*archive\\b[^)]*$`i', $createTableSql) !== 1;
	}
}