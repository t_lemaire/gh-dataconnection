<?php
/**
 * A class/service for wrapping PostgreSQL's procedural functions
 * The goal is to be as lightweight as possible and reduce overhead
 * User: e_pickup
 * Date: 07/11/13
 */

namespace GorillaHub\DataConnectionBundle;
use \GorillaHub\DataConnectionBundle\Exceptions\QueryException;
use GorillaHub\DataConnectionBundle\Helpers\LoggerAdapter;
use \GorillaHub\DataConnectionBundle\ResultSets\AbstractResultSet;
use \Symfony\Bridge\Monolog\Logger;

/**
 * Class PostgreSQLService
 * @package GorillaHub\DataConnectionBundle
 */
abstract class AbstractSQLConnection extends AbstractConnection {
	/**
	 * @var LoggerAdapter
	 */
	private $logger;

	/** @var string Username for connecting to database */
	protected $userName;

	/** @var string Password for connecting to database */
	protected $passWord;

	/** @var string Database to connect to */
	protected $database;

	/** @var string Host for connecting to database */
	protected $host;

	/** @var string Port for connecting to database */
	protected $port;

	/** @var string Character encoding for connecting to database, should always be UTF8!! */
	protected $characterEncoding;

	/** @var array additional options for connecting to database, when it's ssl connection */
	protected $sslOptions;

	/** @var  string time zone for connecting to database */
	protected $timeZone;

	/**
	 * @return string
	 */
	public function getUserName()
	{
		return $this->userName;
	}

	/**
	 * @return string
	 */
	public function getPassWord()
	{
		return $this->passWord;
	}

	/**
	 * @return string
	 */
	public function getDatabase()
	{
		return $this->database;
	}

	/**
	 * @return string
	 */
	public function getHost()
	{
		return $this->host;
	}

	/**
	 * @return string
	 */
	public function getPort()
	{
		return $this->port;
	}

	/**
	 * @return string
	 */
	public function getCharacterEncoding()
	{
		return $this->characterEncoding;
	}

	/**
	 * @return array
	 */
	public function getSslOptions()
	{
		return $this->sslOptions;
	}

	/**
	 * @return string
	 */
	public function getTimeZone()
	{
		return $this->timeZone;
	}

	/**
	 * Create a new instance of the connection, passing the information needed to connect to the database
	 * the actual connection is not actually created until needed.
	 * @param string $userName
	 * @param string $passWord
	 * @param string $database
	 * @param string $host
	 * @param string $port
	 * @param string $characterEncoding
	 * @param array $sslOptions
	 * @param string $timezone
	 */
	public function __construct($userName, $passWord, $database, $host="", $port="", $characterEncoding="",$sslOptions = array(), $timezone=null) {
		$this->userName          = $userName;
		$this->passWord          = $passWord;
		$this->database          = $database;
		$this->host              = $host;
		$this->port              = $port;
		$this->characterEncoding = $characterEncoding;
		$this->sslOptions        = $sslOptions;
		$this->timeZone          = $timezone;
	}
	/**
	 * Connects to database server using the parameters passed into the constructor
	 * This method should rarely need to be called directly.  Any used of a method requiring a connection to the db
	 * will execute this method if needed.  This method returns immediately, if a valid connection exists.
	 * @throws Exceptions\ConnectionException
	 */
	abstract public function connect();

	/**
	 * Executes an arbitrary query passed as a string.  Will connect to the DB if a valid connection doesn't
	 * already exist.
	 * @param string $query
	 * @return AbstractResultSet
	 * @throws QueryException
	 */
	abstract public function query($query);

    /**
     * Escapes and wraps in quotes the identifier (fields, tables, etc) to the needs of the database engine.
	 *
     * @param string|string[] $objectName The name of the object, or an array of identifiers to be joined together;
	 * 		e.g. array('myschema', 'mytable') becomes "`myschema`.`mytable`".
     * @return string The SQL expression that identifies the object.
     */
    abstract public function escapeIdentifier($objectName);

    /**
     * This method returns an SQL literal that represents the value passed through $value.  In particular:
	 * 		if $value is a string, an SQL string literal that represents the value of $value is returned;
	 * 		if $value is null, an SQL NULL literal is returned;
	 *		if $value is an integer, an SQL integer literal that represents the value of $value is returned;
	 * 		if $value is a float, an SQL floating point literal that approximates the value of $value is returned; and
	 *		if $value is an instance of SQLExpression, that actual expression is returned as a string, without
	 * 			modification.
	 *
     * @param mixed $value The value to convert to an SQL literal.
     * @return string The SQL literal, suitable for inclusion in an SQL statement.
     */
    abstract public function escapeLiteral($value);

	/**
	 * This creates an object that represents a transaction.  {@see AbstractSQLTransaction}
	 *
	 * @return AbstractSQLTransaction
	 */
	abstract public function createTransaction();

	/**
	 * This is used to start a transaction.  This method may be called while a transaction is already in progress.  In
	 * this case, every call to startTransaction() must have a corresponding call to commit() or rollBack() before the
	 * transaction ends.  The transaction will be committed if all calls to startTransaction() have a corresponding
	 * call to commit().  If there is at least one call to rollBack(), the entire transaction is rolled
	 * back.
	 *
	 * @return int An integer that uniquely identifies this transaction, within this session.  In the case of nested
	 * 		transactions, all simultaneous transactions (i.e. all transactions that appear to be the same transaction
	 * 		from the perspective of the database) have the same ID.
	 */
	abstract public function startTransaction();

	/**
	 * This commits a transaction.
	 * @see startTransaction()
	 */
	abstract public function commit();

	/**
	 * This rolls back a transaction.
	 * @see startTransaction()
	 */
	abstract public function rollBack();

	/**
	 * @return bool true if a transaction is in progress, false otherwise.
	 */
	abstract public function isInTransaction();

	/**
	 * Escapes and wraps in quotes an array of identifiers (fields, tables, etc) to the needs of the database engine.
	 *
	 * @param array $objectNames An array where each value is either a string or an array {@see escapeIdentifier()}
	 * @return string[] The SQL expressions that identify the objects, with the same keys as $objectNames.
	 */
	public function escapeIdentifiers($objectNames) {
		return array_map(array($this, 'escapeIdentifier'), $objectNames);
	}

	/**
	 * Escapes and wraps in quotes an array of literals (values) to the needs of the database engine.
	 * @param array $data
	 * @return array
	 */
	public function escapeLiterals($data) {
		return array_map(array($this, 'escapeLiteral'), $data);
	}

	/**
	 * Given a table names, hash of fields/values, returns a fully escaped insert statement
	 * @param string|array $tableIdentifier {@see escapeIdentifier()}
	 * @param array  $values
	 * @param string $action defaults to "INSERT", could also be "REPLACE"
	 * @param array  $onDuplicateUpdateColumns columns to update on duplicate keys (only valid with INSERT $action)
	 * @return string
	 * @throws \LogicException if the parameters are not correct.
	 */
	public function prepareInsert($tableIdentifier, array $values, $action = 'INSERT', array $onDuplicateUpdateColumns = array()) {

		if (empty($values)) {
			throw new \LogicException("Passed an empty array to prepareInsert.");
		}
		$firstRow = reset($values);
		$extra = '';

		if (!is_array($firstRow)) {
			$values = array($values);
		}

		$columns = array();
		foreach ($values as $row) {
			foreach ($row as $key => $value) {
				$columns[$key] = $key;
			}
		}

		$valueSets = array();
		foreach ($values as $row) {
			$valueSet = '';
			$separator = '';
			foreach ($columns as $column) {
				$valueSet .= $separator;
				$separator = ', ';
				if (array_key_exists($column, $row)) {
					$valueSet .= $this->escapeLiteral($row[$column]);
				} else {
					$valueSet .= 'DEFAULT';
				}
			}
			$valueSets[] = $valueSet;
		}

		$columns = implode(', ', $this->escapeIdentifiers($columns));

		$_this   = $this;
		if (!empty($onDuplicateUpdateColumns)) {

			$extra = ' ON DUPLICATE KEY UPDATE ' . implode(', ', array_map(function ($field) use ($_this) {

					$field = $_this->escapeIdentifier($field);

					return "{$field} = VALUES({$field})";
				}, $onDuplicateUpdateColumns));
		}

		$quotedTableId = $this->escapeIdentifier($tableIdentifier);

		return "$action INTO " . $quotedTableId . " ( {$columns} ) VALUES ( "
				. implode('), (', $valueSets)
				. " ){$extra}";
	}

	/**
	 * Given a table name, hash of fields/values and the primary key column, returns an update statement
	 *
	 * @param string|array $tableIdentifier {@see escapeIdentifier()}
	 * @param array $values
	 * @param string|string[] $primaryKeyOrKeys
	 * @param string|array $where {@see prepareExpression()}
	 * @return string
	 * @throws QueryException, \LogicException
	 */
	public function prepareUpdate($tableIdentifier, array $values, $primaryKeyOrKeys, $where = []) {
		$primaryKeys = is_array($primaryKeyOrKeys) ? $primaryKeyOrKeys : array($primaryKeyOrKeys);
		if ($primaryKeys === array()) {
			throw new QueryException('Tried to update without specifying any primary keys.');
		}
		foreach ($primaryKeys as $primaryKey) {
			if (!array_key_exists($primaryKey, $values)) {
				throw new QueryException(
					'Tried to update without passing a value for the specified primary key ' . $primaryKey
				);
			}
		}

		$quotedTableId = $this->escapeIdentifier($tableIdentifier);

		foreach ($primaryKeys as $primaryKey) {
			$where[$primaryKey] = $values[$primaryKey];
			unset($values[$primaryKey]);
		}
		$equalitiesSql = $this->prepareExpression($where);

		$assignmentsSql = $this->prepareAssignmentsSql($values);

		return "UPDATE {$quotedTableId} SET {$assignmentsSql} WHERE {$equalitiesSql}";
	}

	/**
	 * Given a table name, hash of fields/values and the primary key column, returns an update statement that
	 * will update the entire table.
	 *
	 * @param string|array $tableIdentifier {@see escapeIdentifier()}
	 * @param array $values
	 * @param array|string|null $where {@see prepareExpression()}
	 * @return string
	 * @throws QueryException, \LogicException
	 */
	public function prepareUpdateEntireTable($tableIdentifier, array $values, $where = null) {
		$sql = 'UPDATE ' . $this->escapeIdentifier($tableIdentifier) . ' SET ' . $this->prepareAssignmentsSql($values);
		if ($where !== null) {
			$sql .= ' WHERE ' . $this->prepareExpression($where);
		}
		return $sql;
	}

	/**
	 * This method updates the specified table with the specified values on the rows with matching primary key.
	 *
	 * @param string|array $tableIdentifier {@see escapeIdentifier()}
	 * @param array $values
	 * @param string|string[] $primaryKeyOrKeys
	 * @param string|array $where {@see prepareExpression()}
	 * @return AbstractResultSet
	 * @throws QueryException, \LogicException
	 */
	public function update($tableIdentifier, array $values, $primaryKeyOrKeys, $where = []) {
		return $this->query($this->prepareUpdate($tableIdentifier, $values, $primaryKeyOrKeys, $where));
	}

	/**
	 * This method updates the entire specified table with the specified values.
	 *
	 * @param string|array $tableIdentifier {@see escapeIdentifier()}
	 * @param array $values
	 * @param string|array|null $where {@see prepareExpression()}
	 * @return AbstractResultSet
	 * @throws QueryException, \LogicException
	 */
	public function updateEntireTable($tableIdentifier, array $values, $where = null) {
		return $this->query($this->prepareUpdateEntireTable($tableIdentifier, $values, $where));
	}

	/**
	 * This method inserts the specified values into the specified table.
	 *
	 * @param string|array $tableIdentifier {@see escapeIdentifier()}
	 * @param array  $values
	 * @param string $action defaults to "INSERT", could also be "REPLACE"
	 * @param array  $onDuplicateUpdateColumns columns to update on duplicate keys (only valid with INSERT $action)
	 * @return string
	 */
	public function insert(
		$tableIdentifier,
		array $values,
		$action = 'INSERT',
		array $onDuplicateUpdateColumns = array()
	) {
		$sql = $this->prepareInsert($tableIdentifier, $values, $action, $onDuplicateUpdateColumns);
		return $this->query($sql);
	}


	/**
	 * This method deletes the matching rows from the specified table
	 *
	 * @param string|array $tableIdentifier {@see escapeIdentifier()}
	 * @param mixed $where {@see prepareExpression()}
	 */
	public function delete($tableIdentifier, $where) {
		$sql = 'DELETE FROM ' . $this->escapeIdentifier($tableIdentifier) . ' WHERE ' .
				$this->prepareExpression($where);
		$this->query($sql);
	}

	/**
	 * This method inserts the specified rows into the specified table.  Multiple rows can also be passed to insert(),
	 * but the advantage of using this method is that it is explicit about accepting multiple rows, and the meaning
	 * of passing an empty array to $rows is that nothing should be inserted.
	 *
	 * @param string|array $tableIdentifier {@see escapeIdentifier()}
	 * @param array[] $rows
	 * @param string $action defaults to "INSERT", could also be "REPLACE"
	 * @param array $onDuplicateUpdateColumns columns to update on duplicate keys (only valid with INSERT $action)
	 * @return null
	 * @throws \Exception if the value passed to $rows is not zero or more arrays representing rows.
	 */
	public function insertMultiple(
		$tableIdentifier,
		array $rows,
		$action = 'INSERT',
		array $onDuplicateUpdateColumns = array()
	) {
		if ($rows === []) {
			return null;
		}
		if (is_array(reset($rows)) === false) {
			throw new \Exception("Argument \$rows of " . __METHOD__ . " must be an array of arrays.");
		}
		$sql = $this->prepareInsert($tableIdentifier, $rows, $action, $onDuplicateUpdateColumns);
		return $this->query($sql);
	}

	/**
	 * @return int The number of affected rows from the last UPDATE query.
	 */
	abstract public function getNumberOfUpdatedRows();

	/**
	 * @return int The autoincremented value of the last inserted row.
	 */
	abstract public function getLastInsertId();

	/**
	 * @param mixed $where A value that represents the expression.  If this is a string, that string is immediately
	 * 		returned by this function. Otherwise, it is an array, and the elements represent conditions that must all
	 * 		be satisfied.  For each element, the key is the name of a column, and the value is one of the
	 * 		following:
	 * 			- A string, int, float, or null, in which case the value of the column must equal that value.
	 * 			- An SQLExpression, in which case the value of the column must equal the value that results from the
	 * 				evaluation of that SQL expression by the RDBMS.
	 * 			- An array of any combination of the above types, in which case the value of the column must equal at
	 * 				least one of those values.  (If the array is empty, the condition always fails.)
	 * 			- An instance of SQLNot, which encapsulates any of the above types.  In this case, the column must not
	 * 				equal any of the above specified types.
     *          - An instance of ContinuationExpression
	 * @return string The SQL expression that represents the conditions specified by $where.
	 */
	public function prepareExpression($where) {
		if (is_string($where)) {
			return $where;
		}
		if ($where instanceof SQLExpression) {
			return $where->toString($this);
		}
		if ($where === []) {
			return 'TRUE';
		}
		$expression = '';
		$conjunction = '';
		foreach ($where as $column => $value) {
			static $positiveOperators = array('in' => 'IN', 'is' => 'IS', '=' => '=', 'false' => 'FALSE');
			static $negativeOperators = array('in' => 'NOT IN', 'is' => 'IS NOT ', '=' => '!=', 'false' => 'TRUE');
			if ($value instanceof SQLNot) {
				$operators = $negativeOperators;
				$value = $value->getValue();
			} else {
				$operators = $positiveOperators;
			}
			if (is_array($value)) {
				if (empty($value)) {
					$expression .= $conjunction . $operators['false'];
				} else {
					$expression .= $conjunction . $this->escapeIdentifier($column) . ' ' . $operators['in'] . ' (';
					$separator = '';
					foreach ($value as $setMember) {
						$expression .= $separator . $this->escapeLiteral($setMember);
						$separator = ',';
					}
					$expression .= ')';
				}
			} else {
			    if ($value instanceof SQLExpression) {
			        $expression .= $conjunction . $this->escapeLiteral($value);
                } else {
                    $expression .= $conjunction . $this->escapeIdentifier($column) . ' ';
                    $sqlLiteral = $this->escapeLiteral($value);
                    $expression .= $operators[($sqlLiteral === 'NULL') ? 'is' : '='];
                    $expression .= ' ' . $this->escapeLiteral($value);
                }
			}
			$conjunction = ' AND ';
		}
		return $expression;
	}

	/**
	 * @param string|string[]|SQLExpression $caseIdentifier Identifier of the case-value {@see escapeIdentifier()},
	 * 		or an SQLExpression
	 * @param string[] $alternateValues An array where, for each element, the value is what the case statement
	 * 		evaluates to if the case-value identified by $caseIdentifier equals the key of the element.
	 * @return string A string in the form 'CASE ... WHEN ... THEN ... END'
	 */
	public function prepareCase($caseIdentifier, $alternateValues) {
		$sql = 'CASE ';
		$sql .= ($caseIdentifier instanceof SQLExpression)
				? (string)$caseIdentifier
				: $this->escapeIdentifier($caseIdentifier);
		foreach ($alternateValues as $caseValue => $resultValue) {
			$sql .= ' WHEN ' . $this->escapeLiteral($caseValue) . ' THEN ' . $this->escapeLiteral($resultValue);
		}
		return $sql . ' END';
	}

	/**
	 * @param array $values An array where, for each element, the key is a column name, and the value is the value
	 * 		that should be stored into that column.
	 * @return string A series of comma-separated SQL assignments, e.g. "`name1` = 'value1', `name2` = 'value2'".
	 */
	public function prepareAssignmentsSql($values) {
		$assignmentSqls = array();
		foreach ($values as $column => $value) {
			$assignmentSqls[] = $this->escapeIdentifier($column) . ' = ' . $this->escapeLiteral($value);
		}
		return implode(', ', $assignmentSqls);
	}

	/**
	 * @param string $message
	 * @param array $data
	 */
	protected function log($message, array $data) {
		if (is_object($this->logger)) {
			$this->logger->addDebug($message, $data);
		}
	}

	/**
	 * @param Logger|null $logger
	 */
	public function setLogger(Logger $logger = null) {
		$this->logger = new LoggerAdapter($logger);
	}

	/**
	 * @param string $databaseName
	 * @throws QueryException if the database could not be selected.
	 */
	abstract public function selectDatabase($databaseName);

}
